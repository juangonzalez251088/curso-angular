import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { TravelDestinationComponent } from './components/travel-destination/travel-destination.component';
import { DestinationsListComponent } from './components/destinations-list/destinations-list.component';
import { DestinationDetailComponent } from './components/destination-detail/destination-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormTravelDestinationComponent } from './components/form-travel-destination/form-travel-destination.component';
import { initializeTravelDestinationState, InitMyDataAction, reducerTravelDestination, TravelDestinationEffects, TravelDestinationState } from './models/TravelDestinationState.model';
import { ActionReducerMap, Store, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UserLoggedInGuard } from './guards/login-user/user-logged-in.guard';
import { AuthService } from './services/auth.service';
import { FlightsComponent } from './components/flights/flights-component/flights-component.component';
import { FlightsMainComponent} from './components/flights/flights-main-component/flights-main-component.component';
import { FlightsSeeMoreComponent } from './components/flights/flights-see-more-component/flights-see-more-component.component';
import { FlightsDetailComponent} from './components/flights/flights-detail-component/flights-detail-component.component';
import { ReservationModule } from './reservation/reservation.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie from 'dexie';
import { TravelDestination } from './models/TravelDestination.model';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core'; 
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpyMeDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive'

export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const childrenFlightsRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: FlightsMainComponent },
  { path: 'see-more', component: FlightsSeeMoreComponent },
  { path: ':id', component: FlightsDetailComponent }
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: DestinationsListComponent },
  { path: 'destination/:id', component: DestinationDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [ UserLoggedInGuard ]},
  { path: 'flights', component: FlightsComponent, canActivate: [ UserLoggedInGuard ], children: childrenFlightsRoutes }
];

//Redux init 
export interface AppState {
  destinations: TravelDestinationState;
}

const reducers: ActionReducerMap<AppState> = {
  destinations: reducerTravelDestination
};

let reducerInitialState = {
  destinations: initializeTravelDestinationState()
};
//Redux fin init

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeTravelDestinationState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeTravelDestinationState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'security-token'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({ providedIn: 'root' })
export class MyDatabase extends Dexie {
  destinations: Dexie.Table<TravelDestination, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({ destinations: '++id, name, imageUrl'});
    this.version(2).stores({ destinations: '++id, name, imageUrl', translations: '++id, lang, key, value' });
  }
}

export const Database = new MyDatabase();

class TranslationLoader implements TranslateLoader {
  constructor( private http: HttpClient){}

  getTranslation(lang: string): Observable<any>{
    const promise = Database.translations.where('lang')
                                            .equals(lang)
                                            .toArray()
                                            .then(results => {
                                              if(results.length === 0){
                                                return this.http
                                                  .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                                                  .toPromise()
                                                  .then(apiResults => {
                                                    Database.translations.bulkAdd(apiResults);
                                                    return apiResults;
                                                  });
                                              }
                                              return results;
                                            }).then((traducciones) => {
                                              console.log('Traducciones cargadas: ');
                                              console.log(traducciones);
                                              return traducciones;
                                            }).then((traducciones) => {
                                              return traducciones.map((t) => ({ [t.key]: t.value }));
                                            });
   return from(promise).pipe(flatMap((elems) => from(elems)));                                       
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    TravelDestinationComponent,
    DestinationsListComponent,
    DestinationDetailComponent,
    FormTravelDestinationComponent,
    LoginComponent,
    ProtectedComponent,
    FlightsComponent,
    FlightsMainComponent,
    FlightsSeeMoreComponent,
    FlightsDetailComponent,
    SpyMeDirective,
    TrackearClickDirective

  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { 
      initialState: reducerInitialState,
      runtimeChecks: { 
        strictActionImmutability: false,
        strictStateImmutability: false
      }
    }),
    EffectsModule.forRoot([TravelDestinationEffects]),
    StoreDevtoolsModule.instrument(),
    ReservationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService, UserLoggedInGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService, { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
