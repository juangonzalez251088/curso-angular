import {
    reducerTravelDestination,
    TravelDestinationState,
    initializeTravelDestinationState,
    InitMyDataAction,
    NewDestinationAction
  } from './TravelDestinationState.model';
  import { TravelDestination } from './TravelDestination.model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
      const prevState: TravelDestinationState = initializeTravelDestinationState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
      const newState: TravelDestinationState = reducerTravelDestination(prevState, action);
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].name).toEqual('destino 1');
    });
  
    it('should reduce new item added', () => {
      const prevState: TravelDestinationState = initializeTravelDestinationState();
      const action: NewDestinationAction = new NewDestinationAction(new TravelDestination('barcelona', 'url'));
      const newState: TravelDestinationState = reducerTravelDestination(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].name).toEqual('barcelona');
    });
  });