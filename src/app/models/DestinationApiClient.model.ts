import { TravelDestination } from './TravelDestination.model';
import { AppConfig, AppState, APP_CONFIG, Database } from '../app.module';
import { Store } from '@ngrx/store';
import { NewDestinationAction, SelectedFavoriteAction } from './TravelDestinationState.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinationApiClient{

    destinations: TravelDestination[] = [];

    constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient){
        this.store
        .select(state => state.destinations)
        .subscribe((data) => {
            console.log('destinos sub store');
            console.log(data);
            this.destinations = data.items;
        });
        this.store
        .subscribe((data) => {
            console.log('all store');
            console.log(data);
        });
    }

    Add(destination: TravelDestination){
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'security-token'});
        const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { new: destination.name }, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if(data.status === 200){
                this.store.dispatch(new NewDestinationAction(destination));
                const myDB = Database;
                myDB.destinations.add(destination);
                console.log('Todos los destinos de las DB');
                myDB.destinations.toArray().then(destinations => console.log(destinations));
            }           
        });
    }

    GetById(id: String): TravelDestination {
        return this.destinations.filter( function(d) { return d.id.toString() === id; })[0];
    }

    GetAll(): TravelDestination[]{
        return this.destinations;
     }

    SelectTravelDestination(destination: TravelDestination){
       this.store.dispatch(new SelectedFavoriteAction(destination));
    }
}